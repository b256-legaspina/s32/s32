const http = require("http");

const port = 4000;

const server = http.createServer(function (request, response){

    if(request.url == "/" && request.method == "GET") {

        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end('Welcome to booking system')
    }
    if(request.url == "/profile" && request.method == "GET") {

        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end('Welcome to your profile!');
    }
    if(request.url == "/courses" && request.method == "GET") {

        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Here's our courses available");
    }
    if(request.url == "/addcourse" && request.method == "POST") {

        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Update a course to our resources:");
    }
    if(request.url == "/updatecourse" && request.method == "PUT") {

        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Update a course to our resources");
    }
    if(request.url == "/archivecourses" && request.method == "DELETE") {

        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Archive courses to our resource");
    }

    
});

server.listen(port);

console.log(`successfully running at port ${port}`);